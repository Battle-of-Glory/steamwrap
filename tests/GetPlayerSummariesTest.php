<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\SteamUser\SteamPlayer;
use SteamWrap\Filter\SteamUser\GetPlayerSummariesFilter;
use SteamWrap\Tests\Util\TestSteamWrap;

class GetPlayerSummariesTest extends TestCase {

    public function test_Result() {
        $client = TestSteamWrap::create();
        $result = $client->getSteam()->getPlayerSummaries(GetPlayerSummariesFilter::steamId("76561198027123396"));

        $this->assertContainsOnlyInstancesOf(SteamPlayer::class, $result->players);
        $this->assertCount(1, $result->players);
    }

    public function test_ResultInvalidSteamId() {
        $client = TestSteamWrap::create();
        $result = $client->getSteam()->getPlayerSummaries(GetPlayerSummariesFilter::steamId("0"));

        $this->assertContainsOnlyInstancesOf(SteamPlayer::class, $result->players);
        $this->assertCount(0, $result->players);
    }

    public function test_ResultMulti() {
        $client = TestSteamWrap::create();
        $result = $client->getSteam()->getPlayerSummaries(GetPlayerSummariesFilter::steamIds(["76561198027123396", "76561198027123393"]));

        $this->assertContainsOnlyInstancesOf(SteamPlayer::class, $result->players);
        $this->assertGreaterThan(1, count($result->players));
    }
}