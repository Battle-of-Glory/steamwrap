<?php
namespace SteamWrap\Tests;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use SteamWrap\Exception\HttpClientRequestException;
use SteamWrap\Http\Guzzle\GuzzleClient;
use SteamWrap\Http\UnifiedClient;

class GuzzleClientTest extends TestCase {
    /**
     * @return GuzzleClient
     */
    private function createClient() {
        return UnifiedClient::create(new GuzzleClient(new Client()));
    }

    public function testSuccessRequest() {
        $client = $this->createClient();
        $response = $client->request("GET", "http://www.google.de");
    }

    public function testInvalidRequest() {
        $client = $this->createClient();
        try {
            $response = $client->request("GET", "http://google.de/404");
            throw new \Exception("Should not happen");
        } catch (HttpClientRequestException $ex) {
            $this->assertEquals(404, $ex->getCode());
        }
    }
}