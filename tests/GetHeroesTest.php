<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Data\Dota2\GameItem;
use SteamWrap\Data\Dota2\Hero;
use SteamWrap\Data\Dota2\Result\GetGameItemsResult;
use SteamWrap\Data\Dota2\Result\GetHeroesResult;
use SteamWrap\Data\Dota2\Result\GetMatchDetailsResult;
use SteamWrap\Exception\SteamException;
use SteamWrap\Exception\ValidationException;
use SteamWrap\Filter\Dota2\GetGameItemsFilter;
use SteamWrap\Filter\Dota2\GetHeroesFilter;
use SteamWrap\Filter\Dota2\GetMatchDetailsFilter;
use SteamWrap\SteamWrap;
use SteamWrap\Tests\Util\TestSteamWrap;

require_once "vendor/autoload.php";

class GetHeroesTest extends TestCase {
    private function getAndAssertHeroes(SteamWrap $client, GetHeroesFilter $filter = null) {
        $detail = $client->getDota2()->getHeroes($filter);
        $this->assertInstanceOf(GetHeroesResult::class, $detail);
        $this->assertContainsOnlyInstancesOf(Hero::class, $detail->heroes);

        return $detail;
    }

    public function testResult() {
        $client = TestSteamWrap::create();
        $this->getAndAssertHeroes($client);
    }

    public function testLocalNameSet() {
        $client = TestSteamWrap::create();
        $res = $this->getAndAssertHeroes($client, GetHeroesFilter::create()->setLanguage("en"));

        foreach ($res->heroes as $hero) {
            $this->assertInternalType("string", $hero->localizedName);
        }
    }
}