<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Data\Dota2\Result\GetMatchHistoryBySequenceNumResult;
use SteamWrap\Enum\EResult;
use SteamWrap\Exception\SteamException;
use SteamWrap\Filter\Dota2\GetMatchHistoryBySequenceNumFilter;
use SteamWrap\Filter\Dota2\GetMatchHistoryFilter;
use SteamWrap\Tests\Util\TestSteamWrap;

require_once "vendor/autoload.php";

class GetMatchHistoryBySequenceNumTest extends TestCase {
    public function testResult() {
        $client = TestSteamWrap::create();
        $result = $client->getDota2()->getMatchHistoryBySequenceNum();
        $this->assertInstanceOf(GetMatchHistoryBySequenceNumResult::class, $result);
        $this->assertContainsOnlyInstancesOf(DetailedMatch::class, $result->matches);
    }

    public function testInvalidMatchesRequestedParam() {
        $client = TestSteamWrap::create();

        try {
            $client->getDota2()->getMatchHistoryBySequenceNum(GetMatchHistoryBySequenceNumFilter::create()->setMatchesRequested(0));
            throw new \Exception("Should not happen");
        } catch(SteamException $ex) {
            $this->assertEquals(EResult::InvalidParam, $ex->getCode());
        }
    }
}