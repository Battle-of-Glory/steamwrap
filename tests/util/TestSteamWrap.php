<?php
namespace SteamWrap\Tests\Util;
use GuzzleHttp\Promise\Promise;
use SteamWrap\Exception\HttpClientRequestException;
use SteamWrap\Http\IHttpClient;
use SteamWrap\Http\IHttpResponse;
use SteamWrap\SteamWrap;

class TestSteamWrap extends SteamWrap {
    public function __construct($resourceDirectory)
    {
        parent::__construct("TEST", $this->createRouterClient($resourceDirectory));
    }

    /**
     * This method creates a dummy api client which should simulate the behaviour of the Steam API.
     * @param $resourceDirectory
     * @return TestClient
     */
    private function createRouterClient($resourceDirectory) {
        $client =  new TestClient($resourceDirectory);

        $client->route("/IDOTA2Match_570/GetMatchHistory/v1/", function($urlData, $query) {
            if (isset($query["account_id"]) && $query["account_id"] == 1) {
                return FileResponse::resource("Dota2/GetMatchHistory/InvalidAccount.json");
            }

            return FileResponse::resource("Dota2/GetMatchHistory/NoFilter.json");
        });

        $client->route("/IDOTA2Match_570/GetMatchDetails/v1/", function($urlData, $query) {
            if (!isset($query["match_id"])) {
                throw HttpClientRequestException::create("Bad Request", 400);
            }

            $res = FileResponse::resource("Dota2/GetMatchDetails/Detail/" . $query["match_id"] . ".json");
            if ($res) {
                return $res;
            } else {
                return FileResponse::resource("Dota2/GetMatchDetails/MatchIDNotFound.json");
            }

        });

        $client->route("/IDOTA2Match_570/GetMatchHistoryBySequenceNum/v0001/", function($urlData, $query) {
            if (isset($query["matches_requested"]) && $query["matches_requested"] <= 0) {
                return FileResponse::resource("Dota2/GetMatchHistoryBySequenceNum/InvalidParam.json");
            }

            return FileResponse::resource("Dota2/GetMatchHistoryBySequenceNum/NoFilter.json");
        });

        $client->route("/ISteamUser/GetPlayerSummaries/v0002/", function($urlData, $query) {
            if (!isset($query["steamids"])) {
                throw HttpClientRequestException::create("Bad Request", 400);
            }

            $singleSteamIds = explode(",", $query["steamids"]);
            if (count($singleSteamIds) > 1) {
                return FileResponse::resource("Steam/GetPlayerSummaries/Multi.json");
            } else if (count($singleSteamIds) == 1) {
                if ($singleSteamIds[0] != "0") {
                    return FileResponse::resource("Steam/GetPlayerSummaries/Single.json");
                }
            }

            return FileResponse::resource("Steam/GetPlayerSummaries/None.json");
        });

        $client->route("/IEconDOTA2_570/GetGameItems/v1/", function($urlData, $query) {
            if (!empty($query["language"])) {
                return FileResponse::resource("Dota2/GetGameItems/LanguageSet.json");
            }

            return FileResponse::resource("Dota2/GetGameItems/Default.json");
        });

        $client->route("/IEconDOTA2_570/GetHeroes/v1/", function($urlData, $query) {
            if (!empty($query["language"])) {
                return FileResponse::resource("Dota2/GetHeroes/LanguageSet.json");
            }

            return FileResponse::resource("Dota2/GetHeroes/Default.json");
        });

        return $client;
    }

    public static function create() {
        return new TestSteamWrap(__DIR__."/../resources");
    }
}

class TestClient implements IHttpClient
{
    private $resourceDirectory;
    private $routes = [];

    public function __construct($resourceDirectory)
    {
        $this->resourceDirectory = $resourceDirectory;
    }

    public function route($path, Callable $callback) {
        $this->routes[$path] = $callback;
    }

    /**
     * @param $method
     * @param $url
     * @return mixed
     * @throws HttpClientRequestException
     */
    public function request($method, $url)
    {
        $urlData = parse_url($url);
        $path = $urlData["path"];
        parse_str($urlData["query"], $query);

        if (isset($this->routes[$path])) {
            $callback = $this->routes[$path];
            $response = $callback($urlData, $query);

            if ($response instanceof IHttpResponse) {
                return $response;
            }
        }

        throw HttpClientRequestException::createFromForeignException(new \Exception("404 Error (".$path.")", 404));
    }

    /**
     * @param $method
     * @param $url
     *
     * @return mixed
     */
    public function requestAsync($method, $url)
    {
        $promise = new Promise();

        try {
            $response = $this->request($method, $url);
            $promise->resolve($response);
        } catch (\Exception $ex) {
            $promise->reject($ex);
        }

        return $promise;
    }
}

class FileResponse implements IHttpResponse {
    private $path;
    public function __construct($path)
    {
        $this->path = $path;
    }

    public function getJSON()
    {
        return \json_decode(file_get_contents($this->path));
    }

    public static function resource($path) {
        $url = __DIR__ . "/../resources/" . $path;
        if (file_exists($url)) {
            return new FileResponse($url);
        } else {
            return null;
        }
    }
}