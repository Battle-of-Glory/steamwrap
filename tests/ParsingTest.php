<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Data\Dota2\Result\GetGameItemsResult;
use SteamWrap\Data\Dota2\Result\GetHeroesResult;
use SteamWrap\Data\Dota2\Result\GetMatchHistoryBySequenceNumResult;
use SteamWrap\Data\Dota2\Result\GetMatchHistoryResult;
use SteamWrap\Data\SteamUser\Result\GetPlayerSummariesResult;
use SteamWrap\Tests\Util\FileResponse;
use SteamWrap\Util\IEquatable;

require_once "vendor/autoload.php";

class ParsingTest extends TestCase {
    private function loadResource($resPath) {
        return FileResponse::resource($resPath);
    }

    public function testDetailedMatch() {
        $json = $this->loadResource("Dota2/GetMatchDetails/Detail/3167445471.json")->getJSON()->result;
        $match = DetailedMatch::fromJSONObject($json);

        $this->deepCheckEntity($json, $match);
    }

    public function test_GetMatchHistoryBySequenceNumResult() {
        $json = $this->loadResource("Dota2/GetMatchHistoryBySequenceNum/NoFilter.json")->getJSON()->result;
        $parsed = GetMatchHistoryBySequenceNumResult::fromJSONObject($json);

        $this->deepCheckEntity($json, $parsed);
    }

    public function test_GetMatchHistoryResult() {
        $json = $this->loadResource("Dota2/GetMatchHistory/NoFilter.json")->getJSON()->result;
        $parsed = GetMatchHistoryResult::fromJSONObject($json);

        $this->deepCheckEntity($json, $parsed);
    }

    public function test_GetPlayerSummariesResult() {
        $json = $this->loadResource("Steam/GetPlayerSummaries/Single.json")->getJSON()->response;
        $parsed = GetPlayerSummariesResult::fromJSONObject($json);

        $this->deepCheckEntity($json, $parsed, [], function($key) {
            $conversionMap = [
                "steamid" => "steamId",
                "personaname" => "personaName",
                "profileurl" => "profileUrl",
                "avatarmedium" => "avatarMedium",
                "avatarfull" => "avatarFull",
                "personastate" => "personaState",
                "communityvisibilitystate" => "communityVisibilityState",
                "profilestate" => "profileState",
                "lastlogoff" => "lastLogOff",
                "commentpermission" => "commentPermission",

                "realname" => "realName",
                "primaryclanid" => "primaryClanId",
                "timecreated" => "timeCreated",
                "gameid" => "gameId",
                "gameserverip" => "gameServerIp",
                "gameextrainfo" => "gameExtraInfo",
                "cityid" => "cityId",
                "loccountrycode" => "locCountryCode",
                "locstatecode" => "locStateCode",
                "loccityid" => "locCityId",

                "personastateflags" => "personaStateFlags"
            ];

            if (isset($conversionMap[$key])) {
                return $conversionMap[$key];
            } else {
                return $key;
            }
        });
    }

    public function test_GetGameItems() {
        $json = $this->loadResource("Dota2/GetGameItems/Default.json")->getJSON()->result;
        $parsed = GetGameItemsResult::fromJSONObject($json);

        $this->deepCheckEntity($json, $parsed);
    }

    public function test_GetHeroes() {
        $json = $this->loadResource("Dota2/GetHeroes/Default.json")->getJSON()->result;
        $parsed = GetHeroesResult::fromJSONObject($json);

        $this->deepCheckEntity($json, $parsed);
    }

    private function deepCheckEntity($json, BaseData $match, array $skip = [], Callable $converter = null) {
        if ($converter == null) {
            $converter = function($key) {
                return $this->dashesToCamelCase($key);
            };
        }

        foreach ($json as $key => $value) {
            $camelCaseKey = $converter($key);

            if (!isset($match->$camelCaseKey)) {
                throw new \Exception("'$key' exists on JSON object, but not on Data Object ".get_class($match)." (Expected: '".$camelCaseKey."')");
            }

            $parsedValue = $match->$camelCaseKey;

            if ($parsedValue instanceof BaseData) {
                $this->deepCheckEntity($value, $parsedValue, [], $converter);
            } else if(is_array($value)) {
                $this->deepCheckArray($value, $parsedValue, $converter);
            } else if ($parsedValue instanceof IEquatable) {
                $this->assertTrue($parsedValue->equals($value), "Not equal");
            } else {
                $this->assertSame($value, $parsedValue);
            }
        }
    }

    private function deepCheckArray($jsonArr, $parsedArray, Callable $converter) {
        foreach ($jsonArr as $subKey => $subJson) {
            $subParsed = $parsedArray[$subKey];
            if ($subParsed instanceof BaseData) {
                $this->deepCheckEntity($subJson, $subParsed, [], $converter);
            } else {
                throw new \Exception("Object is not instance of BaseData");
            }
        }
    }

    function dashesToCamelCase($string, $capitalizeFirstCharacter = false)
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }
}
