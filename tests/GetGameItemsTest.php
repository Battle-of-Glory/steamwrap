<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Data\Dota2\GameItem;
use SteamWrap\Data\Dota2\Result\GetGameItemsResult;
use SteamWrap\Data\Dota2\Result\GetMatchDetailsResult;
use SteamWrap\Exception\SteamException;
use SteamWrap\Exception\ValidationException;
use SteamWrap\Filter\Dota2\GetGameItemsFilter;
use SteamWrap\Filter\Dota2\GetMatchDetailsFilter;
use SteamWrap\SteamWrap;
use SteamWrap\Tests\Util\TestSteamWrap;

require_once "vendor/autoload.php";
require_once "util/TestSteamWrap.php";

class GetGameItemsTest extends TestCase {
    private function getAndAssertGameItems(SteamWrap $client, GetGameItemsFilter $filter = null) {
        $detail = $client->getDota2()->getGameItems($filter);
        $this->assertInstanceOf(GetGameItemsResult::class, $detail);
        $this->assertContainsOnlyInstancesOf(GameItem::class, $detail->items);

        return $detail;
    }

    public function testResult() {
        $client = TestSteamWrap::create();
        $this->getAndAssertGameItems($client);
    }

    public function testLocalNameSet() {
        $client = TestSteamWrap::create();
        $res = $this->getAndAssertGameItems($client, GetGameItemsFilter::create()->setLanguage("en"));

        foreach ($res->items as $item) {
            $this->assertInternalType("string", $item->localizedName);
        }
    }
}