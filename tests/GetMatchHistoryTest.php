<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\Dota2\Match;
use SteamWrap\Data\Dota2\Result\GetMatchHistoryResult;
use SteamWrap\Enum\EResult;
use SteamWrap\Exception\SteamException;
use SteamWrap\Filter\Dota2\GetMatchHistoryFilter;
use SteamWrap\Tests\Util\TestSteamWrap;

require_once "vendor/autoload.php";

class GetMatchHistoryTest extends TestCase {
    public function testResult() {
        $client = TestSteamWrap::create();
        $result = $client->getDota2()->getMatchHistory();

        $this->assertInstanceOf(GetMatchHistoryResult::class, $result);
        $this->assertContainsOnlyInstancesOf(Match::class, $result->matches);
    }

    public function testInvalidAccountId() {
        $client = TestSteamWrap::create();

        try {
            $client->getDota2()->getMatchHistory(GetMatchHistoryFilter::create()->setAccountId(1));
            throw new \Exception("Shouldn't happen");
        } catch (SteamException $ex) {
            $this->assertEquals(EResult::AccessDenied, $ex->getCode());
        }
    }
}