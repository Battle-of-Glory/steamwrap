<?php
namespace SteamWrap\Tests;
use PHPUnit\Framework\TestCase;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Data\Dota2\Result\GetMatchDetailsResult;
use SteamWrap\Exception\SteamException;
use SteamWrap\Exception\ValidationException;
use SteamWrap\Filter\Dota2\GetMatchDetailsFilter;
use SteamWrap\Tests\Util\TestSteamWrap;

require_once "vendor/autoload.php";

class GetMatchDetailsTest extends TestCase {
    public function testResult() {
        $client = TestSteamWrap::create();

        $detail = $client->getDota2()->getMatchDetails(GetMatchDetailsFilter::create()->setMatchId(3167445471));
        $this->assertInstanceOf(DetailedMatch::class, $detail);
    }

    public function testMatchIDNotFound() {
        $client = TestSteamWrap::create();

        try {
            $detail = $client->getDota2()->getMatchDetails(GetMatchDetailsFilter::create()->setMatchId(1));
            throw new \Exception("Should not happen");
        } catch (SteamException $ex) {
            $this->assertEquals("Match ID not found (Code: Invalid)", $ex->getMessage());
        }
    }

    public function testNoMatchID() {
        $client = TestSteamWrap::create();

        try {
            $detail = $client->getDota2()->getMatchDetails(GetMatchDetailsFilter::create());
            throw new \Exception("Should not happen");
        } catch (ValidationException $ex) {
            $this->assertEquals("Match ID has to be set", $ex->getMessage());
        }
    }
}