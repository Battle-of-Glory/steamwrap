<?php
namespace SteamWrap\Exception;

class HttpClientRequestException extends WrapperException {
    private $foreign;

    /**
     * @return boolean
     */
    public function isForeign()
    {
        return $this->foreign;
    }

    public function __construct($message, $code, \Exception $previous = null, $isForeign = false)
    {
        parent::__construct($message, $code, $previous);
        $this->foreign = $isForeign;
    }

    public static function createFromForeignException(\Exception $exception) {
        return new HttpClientRequestException("[FOREIGN EXCEPTION] ".$exception->getMessage(), $exception->getCode(), $exception, true);
    }

    public static function create($message, $code) {
        return new HttpClientRequestException($message, $code);
    }
}