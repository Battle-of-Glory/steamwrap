<?php
namespace SteamWrap\Exception;
use Exception;
use SteamWrap\Enum\EResult;

class SteamException extends WrapperException {
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        $nameOfCode = EResult::getName($code);
        parent::__construct($message . " (Code: ".$nameOfCode.")", $code, $previous);
    }
}