<?php
namespace SteamWrap\Http;
class JSONObjectWrapper {
    private $unsafeJSON;
    public function __construct(\stdClass $unsafeJSON)
    {
        $this->unsafeJSON = $unsafeJSON;
    }

    public function __get($name)
    {
        if (isset($this->unsafeJSON->$name)) {
            return $this->unsafeJSON->$name;
        }

        return null;
    }
}