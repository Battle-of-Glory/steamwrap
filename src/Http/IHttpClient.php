<?php
namespace SteamWrap\Http;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;

interface IHttpClient {
    /**
     * @param $method
     * @param $url
     * @return IHttpResponse
     */
    public function request($method, $url);

    /**
     * @param $method
     * @param $url
     *
     * @return PromiseInterface
     */
    public function requestAsync($method, $url);
}