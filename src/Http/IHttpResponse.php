<?php
namespace SteamWrap\Http;

interface IHttpResponse {
    public function getJSON();
}