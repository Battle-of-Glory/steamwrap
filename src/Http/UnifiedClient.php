<?php
namespace SteamWrap\Http;
use GuzzleHttp\Promise\PromiseInterface;
use SteamWrap\Exception\HttpClientRequestException;
use SteamWrap\Exception\WrapperException;

class UnifiedClient implements IHttpClient {
    private $client;
    public function __construct(IHttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param $method
     * @param $url
     * @return IHttpResponse
     * @throws WrapperException
     */
    public function request($method, $url)
    {
        try {
            return $this->client->request($method, $url);
        } catch (\Exception $ex) {
            if ($ex instanceof WrapperException) {
                throw $ex;
            }
            throw HttpClientRequestException::createFromForeignException($ex);
        }
    }

    /**
     * @param IHttpClient $client
     * @return IHttpClient|UnifiedClient
     */
    public static function create(IHttpClient $client) {
        if ($client instanceof UnifiedClient) {
            return $client;
        }
        return new UnifiedClient($client);
    }

    /**
     * @param $method
     * @param $url
     *
     * @return PromiseInterface
     */
    public function requestAsync($method, $url)
    {
        return $this->client->requestAsync($method, $url)->then(function ($response) {
            return $response;
        }, function ($ex) {
            if ($ex instanceof WrapperException) {
                throw $ex;
            }
            throw HttpClientRequestException::createFromForeignException($ex);
        });
    }
}