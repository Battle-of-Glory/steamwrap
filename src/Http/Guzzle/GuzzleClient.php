<?php
namespace SteamWrap\Http\Guzzle;
use SteamWrap\Http\IHttpClient;
use SteamWrap\Http\IHttpResponse;
use GuzzleHttp\Client;

class GuzzleClient implements IHttpClient {
    private $client;
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $method
     * @param $url
     * @return IHttpResponse
     */
    public function request($method, $url)
    {
        return new GuzzleResponse($this->client->request($method, $url, [
            "verify" => false
        ]));
    }

    /**
     * @param $method
     * @param $url
     *
     * @return mixed
     */
    public function requestAsync($method, $url)
    {
        return $this->client->requestAsync($method, $url, [
            "verify" => false
        ])->then(function($response) {
            return new GuzzleResponse($response);
        });
    }
}