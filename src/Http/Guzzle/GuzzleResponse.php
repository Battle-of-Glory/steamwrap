<?php
namespace SteamWrap\Http\Guzzle;
use SteamWrap\Http\IHttpResponse;
use Psr\Http\Message\ResponseInterface;

class GuzzleResponse implements IHttpResponse {
    private $response;
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getJSON()
    {
        $json = json_decode($this->response->getBody());
        if (!$json) {
            throw new \Exception("Error parsing JSON");
        }
        return $json;
    }
}