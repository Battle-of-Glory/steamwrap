<?php
namespace SteamWrap;

use SteamWrap\Filter\IFilter;
use SteamWrap\Http\IHttpClient;
use SteamWrap\Http\UnifiedClient;

class SteamRequester {
    const STEAM_API_BASE_URL = 'https://api.steampowered.com/';

    private $client;
    private $apiKey;

    /**
     * SteamRequester constructor.
     * @param $apiKey
     * @param IHttpClient $client
     */
    public function __construct($apiKey, IHttpClient $client)
    {
        $this->apiKey = $apiKey;
        $this->client = UnifiedClient::create($client);
    }

    /**
     * @param $method
     * @param $namespace
     * @param $endpoint
     * @param IFilter|null $filter
     * @param array $params
     * @return Http\IHttpResponse
     */
    public function request($method, $namespace, $endpoint, IFilter $filter = null, $params = []) {
        $url = $this->createRequestUrl($namespace, $endpoint, $filter, $params);
        return $this->client->request($method, $url);
    }

    /**
     * @param $method
     * @param $namespace
     * @param $endpoint
     * @param IFilter|null $filter
     * @param array $params
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function requestAsync($method, $namespace, $endpoint, IFilter $filter = null, $params = [])
    {
        $url = $this->createRequestUrl($namespace, $endpoint, $filter, $params);
        return $this->client->requestAsync($method, $url);
    }

    /**
     * @param $namespace
     * @param $endpoint
     * @param IFilter|null $filter
     * @param array $params
     * @return string
     */
    protected function createRequestUrl($namespace, $endpoint, IFilter $filter = null, $params = []) {
        if ($filter != null) {
            $filter->validate();
            $filterParams = $filter->getQueryParameters();
            if (is_array($filterParams)) {
                $params = array_merge($filterParams, $params);
            }
        }

        $params["key"] = $this->apiKey;
        $queryString = http_build_query($params);

        $url = self::STEAM_API_BASE_URL . $namespace . "/" . $endpoint . "/?" . $queryString;

        return $url;
    }
}