<?php
namespace SteamWrap\Clients;
use SteamWrap\AbstractSteamSubClient;
use SteamWrap\Data\Dota2\Result\GetPlayerSummariesSimplifiedResult;
use SteamWrap\Data\SteamUser\Result\GetPlayerSummariesResult;
use SteamWrap\Filter\IFilter;
use SteamWrap\Filter\SteamUser\GetPlayerSummariesFilter;
use SteamWrap\Http\IHttpResponse;

class SteamClient extends AbstractSteamSubClient {
    /**
     * @param $method
     * @param $endpoint
     * @param IFilter $filter
     * @param array $params
     * @return \SteamWrap\Http\IHttpResponse
     */
    protected function request($method, $endpoint, IFilter $filter, $params = []) {
        return $this->getSteamRequester()->request($method, "ISteamUser", $endpoint, $filter, $params);
    }

    /**
     * @param $method
     * @param $endpoint
     * @param IFilter $filter
     * @param array $params
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function requestAsync($method, $endpoint, IFilter $filter, $params = []) {
        return $this->getSteamRequester()->requestAsync($method, "ISteamUser", $endpoint, $filter, $params);
    }

    /**
     * @param GetPlayerSummariesFilter|null $filter
     * @return GetPlayerSummariesResult
     */
    public function getPlayerSummaries(GetPlayerSummariesFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetPlayerSummariesFilter();
        }

        $resultBare = $this->request("GET", "GetPlayerSummaries/v0002", $filter)->getJSON()->response;
        $result = GetPlayerSummariesResult::fromJSONObject($resultBare);
        return $result;
    }

    /**
     * @param GetPlayerSummariesFilter|null $filter
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getPlayerSummariesAsync(GetPlayerSummariesFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetPlayerSummariesFilter();
        }

        return $this->requestAsync("GET", "GetPlayerSummaries/v0002", $filter)
            ->then(function (IHttpResponse $response) {
                $resultBare = $response->getJSON()->response;

                $result = GetPlayerSummariesResult::fromJSONObject($resultBare);
                return $result;
            });
    }
}