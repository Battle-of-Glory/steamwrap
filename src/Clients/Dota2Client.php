<?php
namespace SteamWrap\Clients;

use SteamWrap\AbstractSteamSubClient;
use SteamWrap\Data\BaseResult;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Data\Dota2\Result\GetGameItemsResult;
use SteamWrap\Data\Dota2\Result\GetHeroesResult;
use SteamWrap\Data\Dota2\Result\GetMatchDetailsResult;
use SteamWrap\Data\Dota2\Result\GetMatchHistoryBySequenceNumResult;
use SteamWrap\Data\Dota2\Result\GetMatchHistoryResult;
use SteamWrap\Data\Dota2\Result\ResultWithStatus;
use SteamWrap\Enum\EResult;
use SteamWrap\Exception\SteamException;
use SteamWrap\Filter\Dota2\GetGameItemsFilter;
use SteamWrap\Filter\Dota2\GetHeroesFilter;
use SteamWrap\Filter\Dota2\GetMatchDetailsFilter;
use SteamWrap\Filter\Dota2\GetMatchHistoryBySequenceNumFilter;
use SteamWrap\Filter\Dota2\GetMatchHistoryFilter;
use SteamWrap\Filter\IFilter;

class Dota2Client extends AbstractSteamSubClient {
    const DOTA2_APP_ID = 570;

    private function getInterfaceName($sub = "", $prefix = "") {
        return "I".$prefix."DOTA2" . $sub . "_" . self::DOTA2_APP_ID;
    }

    protected function request($method, $sub, $url, IFilter $filter) {

        return $this->getSteamRequester()->request($method, $this->getInterfaceName($sub), $url, $filter);
    }

    protected function requestEcon($method, $endpoint, IFilter $filter) {
        return $this->getSteamRequester()->request($method, $this->getInterfaceName("", "Econ"), $endpoint, $filter);
    }

    /**
     * @param BaseResult $data
     * @throws SteamException
     */
    private function throwIfErrorAttribute(BaseResult $data) {
        if ($data->error) {
            throw new SteamException($data->error);
        }
    }

    private function throwIfInvalidStatus(ResultWithStatus $resultWithStatus) {
        if ($resultWithStatus->status != EResult::OK) {
            throw new SteamException($resultWithStatus->statusDetail ? $resultWithStatus->statusDetail : "Unknown status detail", $resultWithStatus->status);
        }
    }

    /**
     * @param GetMatchHistoryFilter|null $filter
     * @return GetMatchHistoryResult
     * @throws SteamException
     */
    public function getMatchHistory(GetMatchHistoryFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetMatchHistoryFilter();
        }

        $resultBare = $this->request("GET", "Match", "GetMatchHistory/v1", $filter)->getJSON()->result;
        $result = GetMatchHistoryResult::fromJSONObject($resultBare);

        $this->throwIfErrorAttribute($result);
        $this->throwIfInvalidStatus($result);

        return $result;
    }

    /**
     * @param GetMatchDetailsFilter|null $filter
     * @return DetailedMatch
     * @throws SteamException
     */
    public function getMatchDetails(GetMatchDetailsFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetMatchDetailsFilter();
        }

        $resultBare = $this->request("GET", "Match", "GetMatchDetails/v1", $filter)->getJSON()->result;
        $result = GetMatchDetailsResult::fromJSONObject($resultBare);

        $this->throwIfErrorAttribute($result);

        return $result->match;
    }

    /**
     * @param GetMatchHistoryBySequenceNumFilter|null $filter
     * @return GetMatchHistoryBySequenceNumResult
     * @throws SteamException
     */
    public function getMatchHistoryBySequenceNum(GetMatchHistoryBySequenceNumFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetMatchHistoryBySequenceNumFilter();
        }

        $resultBase = $this->request("GET", "Match", "GetMatchHistoryBySequenceNum/v0001", $filter)->getJSON()->result;
        $result = GetMatchHistoryBySequenceNumResult::fromJSONObject($resultBase);

        $this->throwIfErrorAttribute($result);
        $this->throwIfInvalidStatus($result);

        return $result;
    }

    /**
     * @param GetGameItemsFilter|null $filter
     * @return GetGameItemsResult
     * @throws SteamException
     */
    public function getGameItems(GetGameItemsFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetGameItemsFilter();
        }

        $resultBare = $this->requestEcon("GET", "GetGameItems/v1", $filter)->getJSON()->result;
        $result = GetGameItemsResult::fromJSONObject($resultBare);

        $this->throwIfErrorAttribute($result);

        return $result;
    }

    /**
     * @param GetHeroesFilter|null $filter
     * @return GetHeroesResult
     * @throws SteamException
     */
    public function getHeroes(GetHeroesFilter $filter = null) {
        if ($filter == null) {
            $filter = new GetHeroesFilter();
        }

        $resultBare = $this->requestEcon("GET", "GetHeroes/v1", $filter)->getJSON()->result;
        $result = GetHeroesResult::fromJSONObject($resultBare);

        $this->throwIfErrorAttribute($result);

        return $result;
    }
}