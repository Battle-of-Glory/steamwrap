<?php
namespace SteamWrap\Enum\Steam;
use SteamWrap\Enum\EnumBase;

abstract class PersonaState extends EnumBase {
    const Offline = 0;
    const Online = 1;
    const Busy = 2;
    const Away = 3;
    const Snooze = 4;
    const LookingToTrade = 5;
    const LookingToPlay = 6;
}