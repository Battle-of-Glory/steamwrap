<?php
namespace SteamWrap\Enum\Steam;
use SteamWrap\Enum\EnumBase;

abstract class CommunityVisibilityState extends EnumBase {
    const ProfilePrivate = 1;
    const ProfilePublic = 3;
}