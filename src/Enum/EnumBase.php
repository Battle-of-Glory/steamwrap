<?php
namespace SteamWrap\Enum;
use ReflectionClass;

class EnumBase {
    /**
     * @param $code
     * @return mixed
     */

    public static function getName($code) {
        $class = new ReflectionClass(get_called_class());
        $constants = array_flip($class->getConstants());

        return $constants[$code];
    }
}