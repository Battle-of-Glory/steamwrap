<?php
namespace SteamWrap\Enum\Dota2;
abstract class LeaverStatus {
    const None = 0;
    const Disconnected = 1;
    const DisconnectedTooLong = 2;
    const Abandoned = 3;
    const AFK = 4;
    const NeverConnected = 5;
    const NeverConnectedTooLong = 6;
}