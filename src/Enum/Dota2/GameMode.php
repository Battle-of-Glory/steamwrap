<?php
namespace SteamWrap\Enum\Dota2;
use SteamWrap\Enum\EnumBase;

abstract class GameMode extends EnumBase {
    const None = 0;
    const AllPick = 1;
    const CaptainsMode = 2;
    const RandomDraft = 3;
    const SingleDraft = 4;
    const AllRandom = 5;
    const Intro = 6;
    const Diretide = 7;
    const ReverseCaptainsMode = 8;
    const TheGreeviling = 9;
    const Tutorial = 10;
    const MidOnly = 11;
    const LeastPlayed = 12;
    const NewPlayerPool = 13;
    const CompendiumMatchmaking = 14;
    const COOPVsBots = 15;
    const CaptainsDraft = 16;
    const AbilityDraft = 18;
    const AllRandomDeathmatch = 20;
    const OneVsOneMidOnly = 21;
    const RankedMatchmaking = 22;
}