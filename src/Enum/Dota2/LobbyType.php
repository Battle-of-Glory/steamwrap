<?php
namespace SteamWrap\Enum\Dota2;
abstract class LobbyType {
    const Invalid = -1;
    const PublicMatchmaking = 0;
    const Practise = 1;
    const Tournament = 2;
    const Tutorial = 3;
    const COOPWithBots = 4;
    const TeamMatch = 5;
    const SoloQueue = 6;
    const RankedMatchmaking = 7;
    const OneVsOneMid = 8;
}