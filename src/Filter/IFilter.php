<?php
namespace SteamWrap\Filter;
interface IFilter {
    /**
     * @return array
     */
    public function getQueryParameters();
    public function validate();
}