<?php
namespace SteamWrap\Filter\SteamUser;

use SteamWrap\Filter\IFilter;

class GetPlayerSummariesFilter implements IFilter {
    public $steamIds = [];

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        $params = [];

        $steamIdsCommaSeparated = "";
        foreach ($this->steamIds as $steamId) {
            $steamIdsCommaSeparated .= $steamId . ",";
        }

        $steamIdsCommaSeparated = rtrim($steamIdsCommaSeparated, ",");
        if (strlen($steamIdsCommaSeparated) > 0) {
            $params["steamids"] = $steamIdsCommaSeparated;
        }

        return $params;
    }

    /**
     * @param $steamID64
     * @return GetPlayerSummariesFilter
     */
    public function addSteamID64($steamID64) {
        $this->steamIds[] = $steamID64;
        return $this;
    }

    /**
     * @param array $steamID64Arr
     * @return GetPlayerSummariesFilter
     */
    public function setSteamIDs64(array $steamID64Arr) {
        $this->steamIds = $steamID64Arr;
        return $this;
    }

    /**
     * @param $accountId
     * @return GetPlayerSummariesFilter
     */
    public function addAccountID($accountId) {
        // TODO: implement account id adding
        return $this;
    }

    /**
     * @param $steamID64
     * @return GetPlayerSummariesFilter
     */
    public static function steamId($steamID64) {
        $filter = new self();
        $filter->addSteamID64($steamID64);

        return $filter;
    }

    /**
     * @param array $steamID64Arr
     * @return GetPlayerSummariesFilter
     */
    public static function steamIds(array $steamID64Arr) {
        $filter = new self();
        $filter->setSteamIDs64($steamID64Arr);

        return $filter;
    }

    /**
     * @return GetPlayerSummariesFilter
     */
    public static function create() {
        return new self();
    }

    public function validate()
    {
        // nothing to implement
    }
}