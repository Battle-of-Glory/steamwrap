<?php
namespace SteamWrap\Filter\Dota2;
use SteamWrap\Filter\IFilter;
use SteamWrap\Filter\ValidationException;

class GetMatchHistoryFilter implements IFilter {
    public $startAtMatchId = null;
    public $accountId = null;
    public $heroId = null;
    public $skill = null;
    public $minPlayers = null;
    public $leagueId = null;
    public $matchesRequested = null;
    public $tournamentGamesOnly = null;

    /**
     * @param $startAtMatchId
     * @return GetMatchHistoryFilter
     */
    public function setStartAtMatchId($startAtMatchId)
    {
        $this->startAtMatchId = $startAtMatchId;
        return $this;
    }

    /**
     * @param $accountId
     * @return GetMatchHistoryFilter
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @param $heroId
     * @return GetMatchHistoryFilter
     */
    public function setHeroId($heroId)
    {
        $this->heroId = $heroId;
        return $this;
    }

    /**
     * @param $skill
     * @return GetMatchHistoryFilter
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;
        return $this;
    }

    /**
     * @param null $minPlayers
     * @return GetMatchHistoryFilter
     */
    public function setMinPlayers($minPlayers)
    {
        $this->minPlayers = $minPlayers;
        return $this;
    }

    /**
     * @param $leagueId
     * @return GetMatchHistoryFilter
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;
        return $this;
    }

    /**
     * @param $matchesRequested
     * @return GetMatchHistoryFilter
     */
    public function setMatchesRequested($matchesRequested)
    {
        $this->matchesRequested = $matchesRequested;
        return $this;
    }

    /**
     * @param $tournamentGamesOnly
     * @return GetMatchHistoryFilter
     */
    public function setTournamentGamesOnly($tournamentGamesOnly)
    {
        $this->tournamentGamesOnly = $tournamentGamesOnly;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        $params = [];

        if ($this->startAtMatchId !== null) {
            $params["start_at_match_id"] = $this->startAtMatchId;
        }

        if ($this->accountId !== null) {
            $params["account_id"] = $this->accountId;
        }

        if ($this->heroId !== null) {
            $params["hero_id"] = $this->heroId;
        }

        if ($this->skill !== null) {
            $params["skill"] = $this->skill;
        }

        if ($this->minPlayers !== null) {
            $params["min_players"] = $this->minPlayers;
        }

        if ($this->leagueId !== null) {
            $params["league_id"] = $this->leagueId;
        }

        if ($this->matchesRequested !== null) {
            $params["matches_requested"] = $this->matchesRequested;
        }

        if ($this->tournamentGamesOnly !== null) {
            $params["tournament_games_only"] = $this->tournamentGamesOnly;
        }

        return $params;
    }

    /**
     * @return GetMatchHistoryFilter
     */
    public static function create() {
        return new self();
    }

    public static function accountId($accountId) {
        return self::create()->setAccountId($accountId);
    }

    public function validate()
    {
        // nothing to implement
    }
}
