<?php
namespace SteamWrap\Filter\Dota2;
use SteamWrap\Filter\IFilter;

class GetMatchHistoryBySequenceNumFilter implements IFilter {
    public $startAtMatchSeqNum = null;
    public $matchesRequested = null;

    /**
     * @param $startAtMatchSeqNum
     * @return GetMatchHistoryBySequenceNumFilter
     */
    public function setStartAtMatchSeqNum($startAtMatchSeqNum)
    {
        $this->startAtMatchSeqNum = $startAtMatchSeqNum;
        return $this;
    }

    /**
     * @param $matchesRequested
     * @return GetMatchHistoryBySequenceNumFilter
     */
    public function setMatchesRequested($matchesRequested)
    {
        $this->matchesRequested = $matchesRequested;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        $params = [];
        if ($this->matchesRequested !== null) {
            $params["matches_requested"] = $this->matchesRequested;
        }

        if ($this->startAtMatchSeqNum !== null) {
            $params["start_at_match_seq_num"] = $this->startAtMatchSeqNum;
        }

        return $params;
    }

    /**
     * @return GetMatchHistoryBySequenceNumFilter
     */
    public static function create() {
        return new self();
    }

    public function validate()
    {
        // nothing to implement
    }
}