<?php
namespace SteamWrap\Filter\Dota2;
use SteamWrap\Filter\IFilter;

class GetGameItemsFilter implements IFilter {
    public $language = "en_us";

    /**
     * @param $language
     * @return GetGameItemsFilter
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        $params = [];
        if ($this->language !== null) {
            $params["language"] = $this->language;
        }

        return $params;
    }

    /**
     * @return GetGameItemsFilter
     */
    public static function create() {
        return new self();
    }

    public function validate()
    {
        // nothing to implement
    }
}