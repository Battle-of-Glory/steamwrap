<?php
namespace SteamWrap\Filter\Dota2;

use SteamWrap\Exception\ValidationException;
use SteamWrap\Filter\IFilter;
use SteamWrap\Data\Dota2\Match;

class GetMatchDetailsFilter implements IFilter {
    public $matchId = null;

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        $params = [];
        if ($this->matchId !== null) {
            $params["match_id"] = $this->matchId;
        }

        return $params;
    }

    /**
     * @param $matchId
     * @return GetMatchDetailsFilter
     */
    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;
        return $this;
    }

    /**
     * @return GetMatchDetailsFilter
     */
    public static function create() {
        return new self();
    }

    public static function match(Match $match) {
        return self::create()->setMatchId($match->matchId);
    }

    public function validate()
    {
        if ($this->matchId === null) {
            throw new ValidationException("Match ID has to be set");
        }
    }
}