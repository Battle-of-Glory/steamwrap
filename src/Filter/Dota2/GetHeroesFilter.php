<?php
namespace SteamWrap\Filter\Dota2;
use SteamWrap\Filter\IFilter;

class GetHeroesFilter implements IFilter {
    public $language = "en_us";
    public $itemizedOnly = null;

    /**
     * @param $language
     * @return GetHeroesFilter
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @param $itemizedOnly
     * @return GetHeroesFilter
     */
    public function setItemizedOnly($itemizedOnly)
    {
        $this->itemizedOnly = $itemizedOnly;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        $params = [];
        if ($this->language !== null) {
            $params["language"] = $this->language;
        }

        if ($this->itemizedOnly !== null) {
            $params["itemizedonly"] = $this->itemizedOnly;
        }

        return $params;
    }

    /**
     * @return GetHeroesFilter
     */
    public static function create() {
        return new self();
    }

    public function validate()
    {
        // nothing to implement
    }
}