<?php
namespace SteamWrap\Data\SteamUser;
use SteamWrap\Data\BaseData;
use SteamWrap\Http\JSONObjectWrapper;

class SteamPlayer extends BaseData {
    public $steamId;
    public $communityVisibilityState;
    public $profileState;
    public $personaName;
    public $lastLogOff;
    public $profileUrl;
    public $avatar;
    public $avatarMedium;
    public $avatarFull;
    public $personaState;
    public $commentPermission;
    public $personaStateFlags;

    public $realName;
    public $primaryClanId;
    public $timeCreated;
    public $gameId;
    public $gameServerIp;
    public $gameExtraInfo;
    public $cityId;
    public $locCountryCode;
    public $locStateCode;
    public $locCityId;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->steamId                      = $obj->steamid;
        $this->personaName                  = $obj->personaname;
        $this->profileUrl                   = $obj->profileurl;
        $this->avatar                       = $obj->avatar;
        $this->avatarMedium                 = $obj->avatarmedium;
        $this->avatarFull                   = $obj->avatarfull;
        $this->personaState                 = $obj->personastate;
        $this->communityVisibilityState     = $obj->communityvisibilitystate;
        $this->profileState                 = $obj->profilestate;
        $this->lastLogOff                   = $obj->lastlogoff;
        $this->commentPermission            = $obj->commentpermission;
        $this->personaStateFlags            = $obj->personastateflags;



        $this->realName                     = $obj->realname;
        $this->primaryClanId                = $obj->primaryclanid;
        $this->timeCreated                  = $obj->timecreated;
        $this->gameId                       = $obj->gameid;
        $this->gameServerIp                 = $obj->gameserverip;
        $this->gameExtraInfo                = $obj->gameextrainfo;
        $this->cityId                       = $obj->cityid;
        $this->locCountryCode               = $obj->loccountrycode;
        $this->locStateCode                 = $obj->locstatecode;
        $this->locCityId                    = $obj->loccityid;
    }
}