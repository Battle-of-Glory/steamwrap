<?php
namespace SteamWrap\Data\SteamUser\Result;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\SteamUser\SteamPlayer;
use SteamWrap\Http\JSONObjectWrapper;

class GetPlayerSummariesResult extends BaseData {
    /**
     * @var SteamPlayer[]
     */
    public $players;
    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->players = SteamPlayer::fromJSONObjects($obj->players);
    }
}