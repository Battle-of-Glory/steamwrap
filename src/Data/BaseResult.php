<?php
namespace SteamWrap\Data;
use SteamWrap\Http\JSONObjectWrapper;

abstract class BaseResult extends BaseData {
    public $error;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->error = $obj->error;
    }
}