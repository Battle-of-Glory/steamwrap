<?php
namespace SteamWrap\Data\Dota2;
use SteamWrap\Http\JSONObjectWrapper;

class DetailedMatchPlayer extends MatchPlayer {
    public $item0;
    public $item1;
    public $item2;
    public $item3;
    public $item4;
    public $item5;

    public $backpack0;
    public $backpack1;
    public $backpack2;

    public $kills;
    public $deaths;

    public $assists;
    public $leaverStatus;
    public $lastHits;
    public $denies;

    public $goldPerMin;
    public $xpPerMin;

    public $level;
    public $heroDamage;
    public $towerDamage;
    public $heroHealing;

    public $gold;
    public $goldSpent;

    public $scaledHeroDamage;
    public $scaledTowerDamage;
    public $scaledHeroHealing;

    public $abilityUpgrades;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        parent::fillFromJSONObject($obj);

        $this->item0 = $obj->item_0;
        $this->item1 = $obj->item_1;
        $this->item2 = $obj->item_2;
        $this->item3 = $obj->item_3;
        $this->item4 = $obj->item_4;
        $this->item5 = $obj->item_5;

        $this->backpack0 = $obj->backpack_0;
        $this->backpack1 = $obj->backpack_1;
        $this->backpack2 = $obj->backpack_2;

        $this->kills = $obj->kills;
        $this->deaths = $obj->deaths;

        $this->assists = $obj->assists;
        $this->leaverStatus = $obj->leaver_status;
        $this->lastHits = $obj->last_hits;
        $this->denies = $obj->denies;

        $this->goldPerMin = $obj->gold_per_min;
        $this->xpPerMin = $obj->xp_per_min;

        $this->level = $obj->level;
        $this->heroDamage = $obj->hero_damage;
        $this->towerDamage = $obj->tower_damage;
        $this->heroHealing = $obj->hero_healing;

        $this->gold = $obj->gold;
        $this->goldSpent = $obj->gold_spent;

        $this->scaledHeroDamage = $obj->scaled_hero_damage;
        $this->scaledTowerDamage = $obj->scaled_tower_damage;
        $this->scaledHeroHealing = $obj->scaled_hero_healing;
    }
}