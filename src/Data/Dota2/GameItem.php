<?php
namespace SteamWrap\Data\Dota2;
use SteamWrap\Data\BaseData;
use SteamWrap\Http\JSONObjectWrapper;

class GameItem extends BaseData {
    public $id;
    public $name;
    public $cost;
    public $secretShop;
    public $sideShop;
    public $recipe;
    public $localizedName;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->id = $obj->id;
        $this->name = $obj->name;
        $this->cost = $obj->cost;
        $this->secretShop = $obj->secret_shop;
        $this->sideShop = $obj->side_shop;
        $this->recipe = $obj->recipe;
        $this->localizedName = $obj->localized_name;
    }
}