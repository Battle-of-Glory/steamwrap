<?php
namespace SteamWrap\Data\Dota2;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\Dota2\Helper\PlayerSlot;
use SteamWrap\Http\JSONObjectWrapper;

class MatchPlayer extends BaseData {
    public $accountId;
    public $playerSlot;
    public $heroId;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->accountId = $obj->account_id;
        $this->heroId = $obj->hero_id;
        $this->playerSlot = new PlayerSlot($obj->player_slot);
    }
}