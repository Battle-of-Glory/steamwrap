<?php
namespace SteamWrap\Data\Dota2\Result;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\BaseResult;
use SteamWrap\Data\Dota2\Hero;
use SteamWrap\Http\JSONObjectWrapper;

class GetHeroesResult extends BaseResult {
    /**
     * @var Hero[] $matches
     */
    public $heroes;
    public $count;
    public $status;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->heroes = Hero::fromJSONObjects($obj->heroes);
        $this->count  = $obj->count;
        $this->status = $obj->status;
    }
}