<?php
namespace SteamWrap\Data\Dota2\Result;
use SteamWrap\Data\BaseResult;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Http\JSONObjectWrapper;

class GetMatchDetailsResult extends BaseResult {
    /**
     * @var DetailedMatch $match
     */
    public $match;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        parent::fillFromJSONObject($obj);
        $this->match = DetailedMatch::fromJSONObject($obj);
    }
}