<?php
namespace SteamWrap\Data\Dota2\Result;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\BaseResult;
use SteamWrap\Data\Dota2\GameItem;
use SteamWrap\Http\JSONObjectWrapper;

class GetGameItemsResult extends BaseResult {
    /**
     * @var GameItem[] $matches
     */
    public $items;
    public $status;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->items = GameItem::fromJSONObjects($obj->items);
        $this->status = $obj->status;
    }
}