<?php
namespace SteamWrap\Data\Dota2\Result;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\Dota2\Match;
use SteamWrap\Http\JSONObjectWrapper;

class GetMatchHistoryResult extends ResultWithStatus {
    public $status;
    public $statusDetail;

    public $numResults;
    public $totalResults;
    public $resultsRemaining;

    /**
     * @var Match[] $matches
     */
    public $matches;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        parent::fillFromJSONObject($obj);
        $this->numResults = $obj->num_results;
        $this->totalResults = $obj->total_results;
        $this->resultsRemaining = $obj->results_remaining;
        $this->matches = Match::fromJSONObjects($obj->matches);
    }
}