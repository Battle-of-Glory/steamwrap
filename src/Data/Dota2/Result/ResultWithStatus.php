<?php
namespace SteamWrap\Data\Dota2\Result;
use SteamWrap\Data\BaseResult;
use SteamWrap\Http\JSONObjectWrapper;

abstract class ResultWithStatus extends BaseResult {
    public $status;
    public $statusDetail;

    private function setStatusFromJSONObject($obj) {
        $this->status = $obj->status;
        $this->statusDetail = $obj->statusDetail;
    }

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        parent::fillFromJSONObject($obj);
        $this->setStatusFromJSONObject($obj);
    }
}