<?php
namespace SteamWrap\Data\Dota2\Result;
use SteamWrap\Data\BaseData;
use SteamWrap\Data\Dota2\DetailedMatch;
use SteamWrap\Http\JSONObjectWrapper;

class GetMatchHistoryBySequenceNumResult extends ResultWithStatus {
    public $status;

    /**
     * @var DetailedMatch[] $matches
     */
    public $matches;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        parent::fillFromJSONObject($obj); // TODO: Change the autogenerated stub
        $this->matches = DetailedMatch::fromJSONObjects($obj->matches);
    }
}