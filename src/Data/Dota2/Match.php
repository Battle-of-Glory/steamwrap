<?php
namespace SteamWrap\Data\Dota2;
use SteamWrap\Data\BaseData;
use SteamWrap\Http\JSONObjectWrapper;

class Match extends BaseData {
    public $matchId;
    public $matchSeqNum;
    public $startTime;
    public $lobbyType;
    public $radiantTeamId;
    public $direTeamId;
    public $players;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->matchId = $obj->match_id;
        $this->matchSeqNum = $obj->match_seq_num;
        $this->startTime = $obj->start_time;
        $this->lobbyType = $obj->lobby_type;
        $this->radiantTeamId = $obj->radiant_team_id;
        $this->direTeamId = $obj->dire_team_id;
        $this->players = MatchPlayer::fromJSONObjects($obj->players);
    }
}
