<?php
namespace SteamWrap\Data\Dota2;
use SteamWrap\Data\Dota2\Helper\BarracksStatus;
use SteamWrap\Data\Dota2\Helper\TowerStatus;
use SteamWrap\Http\JSONObjectWrapper;

class DetailedMatch extends Match {
    public $players;

    public $radiantWin;
    public $duration;
    public $preGameDuration;

    /**
     * @var TowerStatus
     */
    public $towerStatusRadiant;

    /**
     * @var TowerStatus
     */
    public $towerStatusDire;

    /**
     * @var BarracksStatus
     */
    public $barracksStatusRadiant;

    /**
     * @var BarracksStatus
     */
    public $barracksStatusDire;

    public $cluster;
    public $firstBloodTime;
    public $lobbyType;
    public $humanPlayers;

    public $leagueid;
    public $positiveVotes;
    public $negativeVotes;

    public $gameMode;
    public $flags;

    public $engine;
    public $radiantScore;
    public $direScore;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        parent::fillFromJSONObject($obj);

        /**
         * This block is from the parent variables. We override the players attribute since we have DetailedMatchPlayers.
         */
        $this->players                 = DetailedMatchPlayer::fromJSONObjects($obj->players);


        $this->radiantWin             = $obj->radiant_win;
        $this->duration                = $obj->duration;
        $this->preGameDuration       = $obj->pre_game_duration;




        $this->towerStatusRadiant    = new TowerStatus($obj->tower_status_radiant);
        $this->towerStatusDire       = new TowerStatus($obj->tower_status_dire);

        $this->barracksStatusDire = new BarracksStatus($obj->barracks_status_dire);
        $this->barracksStatusRadiant = new BarracksStatus($obj->barracks_status_radiant);

        $this->cluster                 = $obj->cluster;
        $this->firstBloodTime        = $obj->first_blood_time;

        $this->humanPlayers           = $obj->human_players;

        $this->leagueid                = $obj->leagueid;
        $this->positiveVotes          = $obj->positive_votes;
        $this->negativeVotes          = $obj->negative_votes;

        $this->gameMode               = $obj->game_mode;
        $this->flags                   = $obj->flags;
        $this->engine                  = $obj->engine;

        $this->radiantScore           = $obj->radiant_score;
        $this->direScore              = $obj->dire_score;
    }
}