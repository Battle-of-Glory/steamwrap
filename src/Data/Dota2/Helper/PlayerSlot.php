<?php
namespace SteamWrap\Data\Dota2\Helper;
use SteamWrap\Util\BitHelper;

class PlayerSlot extends BitMaskContainer {
    public $position;
    public $team;

    public function fillObject($decimal)
    {
        $this->position = BitHelper::readDecimalNBits($decimal, 2);
        $this->team     = BitHelper::readNthBit($decimal, 7);
    }
}