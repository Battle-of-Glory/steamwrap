<?php
namespace SteamWrap\Data\Dota2\Helper;
use SteamWrap\Util\BitHelper;

class BarracksStatus extends BitMaskContainer {
    public $topMelee;
    public $topRanged;
    public $middleMelee;
    public $middleRanged;
    public $bottomMelee;
    public $bottomRanged;

    public function fillObject($decimal)
    {
        $this->topMelee = BitHelper::readNthBool($decimal, 0);
        $this->topRanged = BitHelper::readNthBool($decimal, 1);

        $this->middleMelee = BitHelper::readNthBool($decimal, 2);
        $this->middleRanged = BitHelper::readNthBool($decimal, 3);

        $this->bottomMelee = BitHelper::readNthBool($decimal, 4);
        $this->bottomRanged = BitHelper::readNthBool($decimal, 5);
    }
}