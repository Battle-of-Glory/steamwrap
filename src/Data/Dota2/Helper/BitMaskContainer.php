<?php
namespace SteamWrap\Data\Dota2\Helper;
use SteamWrap\Util\IEquatable;

abstract class BitMaskContainer implements IEquatable {
    private $decimal;

    public function __construct($decimal)
    {
        $this->setDecimal($decimal);
    }

    public abstract function fillObject($decimal);
    protected function setDecimal($decimal) {
        $this->decimal = $decimal;
        $this->fillObject($decimal);
    }

    public function getDecimal() {
        return $this->decimal;
    }

    /**
     * @param $obj
     * @return boolean
     */
    public function equals($obj)
    {
        if ($obj instanceof BitMaskContainer) {
            return $this->getDecimal() == $obj->getDecimal();
        }

        if (is_integer($obj)) {
            return $obj == $this->getDecimal();
        }

        return false;
    }

    public function getBits() {
        return decbin($this->decimal);
    }
}