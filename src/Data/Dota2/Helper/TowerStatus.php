<?php
namespace SteamWrap\Data\Dota2\Helper;
use SteamWrap\Util\BitHelper;
use SteamWrap\Util\IEquatable;

class TowerStatus extends BitMaskContainer {
    public $topTier1 = false;
    public $topTier2 = false;
    public $topTier3 = false;

    public $middleTier1 = false;
    public $middleTier2 = false;
    public $middleTier3 = false;

    public $bottomTier1 = false;
    public $bottomTier2 = false;
    public $bottomTier3 = false;

    public function fillObject($pureStatus)
    {
        $this->topTier1         = BitHelper::readNthBool($pureStatus, 0);
        $this->topTier2         = BitHelper::readNthBool($pureStatus, 1);
        $this->topTier3         = BitHelper::readNthBool($pureStatus, 2);

        $this->middleTier1      = BitHelper::readNthBool($pureStatus, 3);
        $this->middleTier2      = BitHelper::readNthBool($pureStatus, 4);
        $this->middleTier3      = BitHelper::readNthBool($pureStatus, 5);

        $this->bottomTier1      = BitHelper::readNthBool($pureStatus, 6);
        $this->bottomTier2      = BitHelper::readNthBool($pureStatus, 7);
        $this->bottomTier3      = BitHelper::readNthBool($pureStatus, 8);
    }
}