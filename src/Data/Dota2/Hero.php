<?php
namespace SteamWrap\Data\Dota2;
use SteamWrap\Data\BaseData;
use SteamWrap\Http\JSONObjectWrapper;

class Hero extends BaseData {
    public $id;
    public $name;
    public $localizedName;

    public function fillFromJSONObject(JSONObjectWrapper $obj)
    {
        $this->id = $obj->id;
        $this->name = $obj->name;
        $this->localizedName = $obj->localized_name;
    }
}