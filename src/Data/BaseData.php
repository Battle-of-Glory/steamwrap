<?php
namespace SteamWrap\Data;

use SteamWrap\Http\JSONObjectWrapper;

abstract class BaseData {
    public abstract function fillFromJSONObject(JSONObjectWrapper $obj);

    /**
     * @param \stdClass|JSONObjectWrapper $obj
     * @return static
     */
    public static function fromJSONObject($obj) {
        if (get_called_class() == BaseData::class) {
            throw new \RuntimeException("Can't call fromJSONObject on BaseData directly. Please use a sub type.");
        }

        if (!$obj instanceof JSONObjectWrapper) {
            $obj = new JSONObjectWrapper($obj);
        }

        $dataObj = new static();
        $dataObj->fillFromJSONObject($obj);

        return $dataObj;
    }

    /**
     * @param $objs
     * @return static[]
     */
    public static function fromJSONObjects($objs) {
        if (!is_array($objs)) {
            return [];
        }

        $arr = [];
        foreach ($objs as $obj) {
            $arr[] = self::fromJSONObject($obj);
        }

        return $arr;
    }
}