<?php
namespace SteamWrap\Util;
interface IEquatable {
    /**
     * @param $obj
     * @return boolean
     */
    public function equals($obj);
}