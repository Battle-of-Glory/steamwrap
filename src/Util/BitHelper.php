<?php
namespace SteamWrap\Util;
class BitHelper {
    public static function readNthBit($decimalNumber, $k) {
        return ($decimalNumber >> $k) & 1;
    }

    public static function readNthBool($decimalNumber, $k) {
        return !!self::readNthBit($decimalNumber, $k);
    }

    public static function readDecimalAt($decimalNumber, $k) {
        return pow(2, $k) * self::readNthBit($decimalNumber, $k);
    }

    public static function readDecimalNBits($decimalNumber, $k) {
        $sum = 0;
        for ($i = 0; $i <= $k; $i++) {
            $sum += self::readDecimalAt($decimalNumber, $i);
        }
        return $sum;
    }
}