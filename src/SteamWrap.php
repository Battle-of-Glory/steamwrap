<?php
namespace SteamWrap;
use GuzzleHttp\Client;
use SteamWrap\Clients\Dota2Client;
use SteamWrap\Clients\SteamClient;
use SteamWrap\Http\Guzzle\GuzzleClient;
use SteamWrap\Http\IHttpClient;

class SteamWrap {
    private $requester;
    private $dota2Client;
    private $steamClient;

    public function __construct($apiKey, IHttpClient $client = null)
    {
        if ($client == null) {
            $client = new GuzzleClient(new Client());
        }

        $this->requester = new SteamRequester($apiKey, $client);
    }

    /**
     * @return Dota2Client
     */
    public function getDota2() {
        if ($this->dota2Client == null) {
            $this->dota2Client = new Dota2Client($this->requester);
        }

        return $this->dota2Client;
    }

    /**
     * @return SteamClient
     */
    public function getSteam() {
        if ($this->steamClient == null) {
            $this->steamClient = new SteamClient($this->requester);
        }

        return $this->steamClient;
    }
}