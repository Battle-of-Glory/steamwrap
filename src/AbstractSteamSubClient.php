<?php
namespace SteamWrap;
abstract class AbstractSteamSubClient {
    private $steamRequester;
    public function __construct(SteamRequester $requester)
    {
        $this->steamRequester = $requester;
    }

    /**
     * @return SteamRequester
     */
    protected function getSteamRequester() {
        return $this->steamRequester;
    }
}